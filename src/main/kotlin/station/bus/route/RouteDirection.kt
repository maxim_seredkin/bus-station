package station.bus.route

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Направление маршрута.
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
enum class RouteDirection {
    /** Прямое. */
    THERETO,

    /** Обратное. */
    THEREFROM;
}