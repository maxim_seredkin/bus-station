package station.bus.route

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Хранилище маршрутов.
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
class RouteStoreImpl : RouteStore {
    companion object {
        /** Маршруты. */
        private val routes: MutableList<Route> = arrayListOf(Route(203, "Харбин", "Магадан"),
                                                             Route(204, "Харбин", "Махачкала"),
                                                             Route(205, "Харбин", "Иволга"),
                                                             Route(206, "Харбин", "Дормамму"))
    }

    override fun create(number: Int, from: String, to: String): Route {
        val route: Route = Route(number, from, to);

        routes.add(route);

        return route;
    }

    override fun getAll() {
        println("Идентификатор".padEnd(15, ' ') +
                "\t" + "Номер маршрута".padEnd(15, ' ') +
                "\t" + "Откуда".padEnd(30, ' ') +
                "\t" + "Куда".padEnd(30, ' '));
        return routes.forEach({
                                  println("${it.id.toString().padEnd(15, ' ')}" +
                                          "\t${it.number.toString().padEnd(15, ' ')}" +
                                          "\t${it.from.padEnd(30, ' ')}" +
                                          "\t${it.to.padEnd(30, ' ')}")
                              });
    }

    override fun get(id: Int): Route {
        return routes.find { it.id == id }
               ?: throw RuntimeException("Route not found!");
    }

    override fun delete(id: Int) {
        routes.removeIf({ it.id == id });
    }
}