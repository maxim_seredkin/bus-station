package station.bus.route

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Маршрут (направление).
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
class Route(/* Номер маршрута. */ val number: Int,
            /* Откуда. */ val from: String,
            /* Куда. */ val to: String) {
    companion object {
        /** Счетчик для получения последовательности идентификаторов. */
        @JvmStatic
        var idCounter: Int = 1;
    }

    /** Идентификатор. */
    val id: Int = idCounter++;
}