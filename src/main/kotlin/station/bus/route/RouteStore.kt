package station.bus.route

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Хранилище маршрутов.
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
interface RouteStore {
    /**
     * Создать маршрут.
     *
     * @param number    номер маршрута.
     * @param from      откуда.
     * @param to        куда.
     *
     * @return маршрут.
     */
    fun create(number: Int,
               from: String,
               to: String): Route;

    /**
     * Получить все.
     *
     *  @return список маршрутов.
     */
    fun getAll();

    /**
     * Получить маршрут.
     *
     * @param id идентификатор.
     *
     * @return маршрут.
     */
    fun get(id: Int): Route;

    /**
     * Удалить.
     *
     * @param id идентификатор.
     */
    fun delete(id: Int);
}
