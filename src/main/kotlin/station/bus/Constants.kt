package station.bus

import java.text.SimpleDateFormat

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Константы программы.
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
class Constants {
    companion object {
        /** Форматир даты. */
        val DATE_TIME_FORMAT: SimpleDateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm");
    }
}