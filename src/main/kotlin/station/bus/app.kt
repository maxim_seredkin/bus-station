package station.bus

import station.bus.commands.Command
import station.bus.commands.ExitCommand
import station.bus.commands.schedule.*

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Точка входа программы.
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
fun main(args: Array<String>) {
    println("Автостанция запущена.");

    val commands: Map<Int, Command<Unit>> = mapOf(Pair(1, PrintAllPassagesCommand()),
                                                  Pair(2, AddPassageToBeginCommand()),
                                                  Pair(3, AddPassageToEndCommand()),
                                                  Pair(4, UpdatePassageCommand()),
                                                  Pair(5, DeletePassageCommand()),
                                                  Pair(6, ExitCommand()))

    while (true) {
        println("Выберите действие:");
        println("1. Вывести список рейсов;");
        println("2. Добавить рейс в начало;");
        println("3. Добавить рейс в конец;");
        println("4. Редактировать рейс;");
        println("5. Удалить рейс;");
        println("6. Выйти.");

        val commandNumber: Int = try {
            readLine()?.toInt() ?: 0;
        } catch (exception: Exception) {
            0;
        };

        commands[commandNumber]?.`do`() ?: println("Команда не найдена! Попробуйте еще раз.");
    };
}