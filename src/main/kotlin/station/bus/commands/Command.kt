package station.bus.commands

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Команда.
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
interface Command<out ReturnT> {
    /**
     * Выполнить команду.
     */
    fun `do`(): ReturnT;
}