package station.bus.commands.route

import station.bus.commands.Command
import station.bus.route.Route
import station.bus.route.RouteStore
import station.bus.route.RouteStoreImpl

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Действие "Выбрать маршрут".
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
class ChooseRouteCommand : Command<Route> {
    override fun `do`(): Route {
        /** Хранилище маршрутов. */
        val routeStore: RouteStore = RouteStoreImpl();

        while (true) {
            print("Введите идентификатор: ");

            try {
                val id: Int = readLine()?.toInt() ?: continue;

                try {
                    return routeStore.get(id);
                } catch (exception: Exception) {
                    println("Маршрут не найден.");
                }
            } catch (exception: Exception) {
                println("Идентификатор не введен.");
            }
        }
    }
}