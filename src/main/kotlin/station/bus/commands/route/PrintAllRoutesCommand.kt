package station.bus.commands.route

import station.bus.commands.Command
import station.bus.route.RouteStore
import station.bus.route.RouteStoreImpl

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Действие "Вывести все маршруты".
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
class PrintAllRoutesCommand : Command<Unit> {
    override fun `do`() {
        /** Хранилище маршрутов. */
        val routeStore: RouteStore = RouteStoreImpl();

        routeStore.getAll();
    }
}