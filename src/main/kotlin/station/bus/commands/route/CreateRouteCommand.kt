package station.bus.commands.route

import station.bus.commands.Command
import station.bus.route.Route
import station.bus.route.RouteStore
import station.bus.route.RouteStoreImpl

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Действие "Создать маршрут".
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
class CreateRouteCommand : Command<Route> {
    override fun `do`(): Route {
        /** Хранилище маршрутов. */
        val routeStore: RouteStore = RouteStoreImpl();

        while (true) {
            println("Введите номер маршрута: ");
            val number: Int = readLine()?.toInt() ?: continue;
            println("Введите откуда: ");
            val from: String = readLine() ?: continue;
            println("Введите куда: ");
            val to: String = readLine() ?: continue;

            return routeStore.create(number, from, to);
        }
    }
}