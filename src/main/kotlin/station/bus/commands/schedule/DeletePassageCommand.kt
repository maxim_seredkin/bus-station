package station.bus.commands.schedule

import station.bus.commands.Command
import station.bus.schedule.Schedule
import station.bus.schedule.ScheduleImpl

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Команда "Удалить рейс".
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
class DeletePassageCommand : Command<Unit> {
    override fun `do`() {
        /** Расписание. */
        val schedule: Schedule = ScheduleImpl();

        print("Введите идентификатор: ");

        try {
            val id: Int = readLine()?.toInt() ?: return;

            schedule.deletePassageById(id);
        } catch (exception: Exception) {
            println("Идентификатор не введен.");
        }
    }
}