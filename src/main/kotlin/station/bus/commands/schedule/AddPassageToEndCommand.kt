package station.bus.commands.schedule

import station.bus.Constants
import station.bus.commands.Command
import station.bus.commands.route.ChooseRouteCommand
import station.bus.commands.route.CreateRouteCommand
import station.bus.commands.route.PrintAllRoutesCommand
import station.bus.passage.Passage
import station.bus.route.Route
import station.bus.route.RouteDirection
import station.bus.schedule.Schedule
import station.bus.schedule.ScheduleImpl
import java.util.*

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Действие "Добавить рейс в начало расписания".
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
class AddPassageToEndCommand : Command<Unit> {
    override fun `do`() {
        /** Расписание. */
        val schedule: Schedule = ScheduleImpl();

        println("Выберите маршрут.");

        val route: Route;

        routeChoice@ while (true) {
            println("Выберите действие:");
            println("1. Вывести список маршрутов;");
            println("2. Выбрать маршрут;");
            println("3. Добавить маршрут;");

            val command: Int = readLine()?.toInt() ?: 0;

            when (command) {
                1 -> PrintAllRoutesCommand().`do`();
                2 -> {
                    route = ChooseRouteCommand().`do`();
                    break@routeChoice;
                };
                3 -> {
                    route = CreateRouteCommand().`do`();
                    break@routeChoice;
                }
            }
        }

        val direction: RouteDirection;
        choiceDirection@ while (true) {
            println("Введите направление маршрута: ");
            println("1. Туда;");
            println("2. Обратно.");
            val direct: Int = readLine()?.toInt() ?: continue@choiceDirection;
            direction = if (direct == 1) {
                RouteDirection.THERETO;
            } else {
                RouteDirection.THEREFROM;
            }
            break;
        }

        var date: Date;
        choiceDate@ while (true) {
            println("Введите дату отправления: ");
            try {
                date = Constants.DATE_TIME_FORMAT.parse(readLine()) ?: continue@choiceDate;
                break;
            } catch (exception: Exception) {
                println("Неправильный формат даты.");
            }
        }

        schedule.addPassageToEnd(Passage(route, direction, date));
    }
}