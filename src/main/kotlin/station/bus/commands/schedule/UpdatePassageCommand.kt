package station.bus.commands.schedule

import station.bus.Constants
import station.bus.commands.Command
import station.bus.commands.route.ChooseRouteCommand
import station.bus.commands.route.CreateRouteCommand
import station.bus.commands.route.PrintAllRoutesCommand
import station.bus.passage.Passage
import station.bus.route.Route
import station.bus.route.RouteDirection
import station.bus.route.RouteStore
import station.bus.route.RouteStoreImpl
import station.bus.schedule.Schedule
import station.bus.schedule.ScheduleImpl

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Команда "Обновить рейс."
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
class UpdatePassageCommand : Command<Unit> {
    override fun `do`() {
        /** Расписание. */
        val schedule: Schedule = ScheduleImpl();

        print("Введите идентификатор: ");

        val passage: Passage;
        try {
            val id: Int = readLine()?.toInt() ?: return;

            try {
                passage = schedule.getPassageById(id);
            } catch (exception: Exception) {
                println("Рейс не найден.");
                return;
            }
        } catch (exception: Exception) {
            println("Идентификатор не введен.");
            return;
        }

        while (true) {
            println("Рейс `${passage.name}`:");
            println("1. Маршрут `${passage.number}`;");
            println("2. Дата отправления `${Constants.DATE_TIME_FORMAT.format(passage.date)}`;");
            println("3. Сохранить.")

            println("Обновить поле: ");

            val commandNumber: Int = try {
                readLine()?.toInt() ?: 0;
            } catch (exception: Exception) {
                0;
            };

            when (commandNumber) {
                1 -> {
                    println("Выберите маршрут.");

                    val route: Route;
                    routeChoice@ while (true) {
                        println("Выберите действие:");
                        println("1. Вывести список маршрутов;");
                        println("2. Выбрать маршрут;");
                        println("3. Добавить маршрут;");

                        val command: Int = readLine()?.toInt() ?: 0;

                        when (command) {
                            1 -> PrintAllRoutesCommand().`do`();
                            2 -> {
                                route = ChooseRouteCommand().`do`();
                                break@routeChoice;
                            };
                            3 -> {
                                route = CreateRouteCommand().`do`();
                                break@routeChoice;
                            }
                        }
                    }

                    val direction: RouteDirection;
                    choiceDirection@ while (true) {
                        println("Введите направление маршрута: ");
                        println("1. Туда;");
                        println("2. Обратно.");

                        val direct: Int = readLine()?.toInt() ?: continue@choiceDirection;
                        direction = if (direct == 1) {
                            RouteDirection.THERETO;
                        } else {
                            RouteDirection.THEREFROM;
                        }
                        break;
                    }

                    // Обновление маршрута.
                    passage.setRoute(route, direction);
                };
                2 -> {
                    choiceDate@ while (true) {
                        println("Введите дату отправления: ");
                        try {
                            val date = Constants.DATE_TIME_FORMAT.parse(readLine()) ?: continue@choiceDate;
                            // Обновление даты отправления.
                            passage.date = date;
                            break;
                        } catch (exception: Exception) {
                            println("Неправильный формат даты.");
                        }
                    }
                };
                // Сохранить.
                3 -> return;
            }
        }
    }
}