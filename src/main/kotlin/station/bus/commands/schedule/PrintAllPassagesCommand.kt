package station.bus.commands.schedule

import station.bus.Constants
import station.bus.commands.Command
import station.bus.schedule.Schedule
import station.bus.schedule.ScheduleImpl
import java.util.*

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Команда "Вывести все рейсы".
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
class PrintAllPassagesCommand : Command<Unit> {
    override fun `do`() {
        /** Расписание. */
        val schedule: Schedule = ScheduleImpl();

        println("Фильтровать по дате? (Y[es]/N[o])");

        val filteredByDate: Boolean = readLine()?.contains("y", true) ?: false;

        var filteredDate: Date? = null;

        if (filteredByDate) {
            println("Введите дату в формате [дд.мм.гггг чч.мм] или now (текущая дата [по умолчанию]): ");

            filteredDate = try {
                val date: String = readLine() ?: "now";
                Constants.DATE_TIME_FORMAT.parse(date);
            } catch (exception: Exception) {
                println("Неправильный формат даты. Текущая дата была задана по умолчанию.");
                Date();
            }
        }

        println("Сортировать по возрастанию даты отправления? (Y[es]/N[o])");

        val sortedByDate: Boolean = readLine()?.contains("y", true) ?: false;

        schedule.printSchedule(filteredDate, sortedByDate);
    }
}