package station.bus.commands

import kotlin.system.exitProcess

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Команда "Завершить работу".
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
class ExitCommand : Command<Unit> {
    override fun `do`() {
        println("Завершение работы.");
        exitProcess(0)
    }
}