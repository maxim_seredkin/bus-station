package station.bus.schedule

import station.bus.passage.Passage
import java.util.*

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Расписание.
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
interface Schedule {
    /**
     * Вывести расписание.
     *
     * @param filteredByDate    фильтровать по дате.
     * @param sortedByDate      сортировать по дате.
     */
    fun printSchedule(filteredByDate: Date?, sortedByDate: Boolean);

    /**
     * Добавить рейс в конец расписания.
     *
     * @param passage рейс.
     */
    fun addPassageToEnd(passage: Passage);

    /**
     * Добавить рейс в начало расписания.
     *
     * @param passage рейс.
     */
    fun addPassageToBegin(passage: Passage);

    /**
     * Получить информацию о рейсе.
     *
     * @param id идентификатор рейса.
     */
    fun getPassageById(id: Int): Passage;

    /**
     * Удалить рейс.
     *
     * @param id идентификатор рейса.
     */
    fun deletePassageById(id: Int);
}