package station.bus.schedule

import station.bus.Constants
import station.bus.passage.Passage
import java.util.*

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Расписание.
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
class ScheduleImpl : Schedule {
    companion object {
        /** Рейсы. */
        private val passages: MutableList<Passage> = arrayListOf()
    }

    override fun printSchedule(filteredByDate: Date?, sortedByDate: Boolean) {
        // Преобразуем в имутабельный лист.
        var passages = passages.toList();

        // Если рейсов нет, то выводим уведомление об этом.
        if (passages.isEmpty()) {
            println("Рейсов нет.");
            return;
        }

        // Если филььтрация по дате задана, то применяем.
        if (filteredByDate != null) {
            passages = passages.filter { !it.date.before(filteredByDate) }
        }

        // Если сортировка по дате задана, то применяем.
        if (sortedByDate) {
            passages = passages.sortedBy { it.date }
        }

        // Выводим информацию на экран.
        println("Идентификатор".padEnd(15, ' ') +
                "\t" + "Номер маршрута".padEnd(15, ' ') +
                "\t" + "Наименование".padEnd(65, ' ') +
                "\t" + "Дата отправления".padEnd(20, ' '));

        passages.forEach {
            run {
                println("${it.id.toString().padEnd(15, ' ')}" +
                        "\t${it.number.toString().padEnd(15, ' ')}" +
                        "\t${it.name.padEnd(65, ' ')}" +
                        "\t${Constants.DATE_TIME_FORMAT.format(it.date).padEnd(20, ' ')}");
            }
        }
    }

    override fun addPassageToEnd(passage: Passage) {
        passages.add(passage);
    }

    override fun addPassageToBegin(passage: Passage) {
        passages.add(0, passage);
    }

    override fun getPassageById(id: Int): Passage {
        return passages.find { it.id == id }
               ?: throw RuntimeException("Passage not found!");
    }

    override fun deletePassageById(id: Int) {
        passages.removeIf({ it.id == id });
    }
}