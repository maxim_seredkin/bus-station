package station.bus.passage

import station.bus.route.Route
import station.bus.route.RouteDirection
import java.util.*

/**
 * Created by Maxim Seredkin on 03.02.2018
 * <p>
 * Рейс.
 *
 * @author Maxim Seredkin
 * @version 1.0.0
 */
class Passage(/* Маршрут.*/ private var route: Route,
              /* Направление маршрута. */ private var direction: RouteDirection,
              /* Дата отправления. */ var date: Date) {
    companion object {
        /** Счетчик для получения последовательности идентификаторов. */
        @JvmStatic
        var idCounter: Int = 1;
    }

    /** Идентификатор. */
    val id: Int = idCounter++;

    /** Номер маршрута. */
    val number: Int
        get() {
            return route.number;
        };

    /** Наименование маршрута. */
    val name: String
        get() {
            return if (direction == RouteDirection.THERETO) "${route.from} - ${route.to}"
            else "${route.to} - ${route.from}";
        };

    /**
     * Изменить маршрут.
     *
     * @param route     машрут.
     * @param direction направление маршрута.
     */
    fun setRoute(route: Route, direction: RouteDirection) {
        this.route = route;
        this.direction = direction;
    }
}